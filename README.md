# [mypy](https://pypi.org/project/mypy/)

Optional static typing for Python

## Official documentation
* [*Welcome to mypy documentation!*
  ](https://mypy.readthedocs.io)
* [*PEP 561 -- Distributing and Packaging Type Information*](https://www.python.org/dev/peps/pep-0561/)

## Unofficial documentation
* [*mypypath: Cannot find module named 'tzwhere'*](https://stackoverflow.com/questions/51206648/mypypath-cannot-find-module-named-tzwhere)

## See also
* [debian-packages-demo/mypy](https://gitlab.com/debian-packages-demo/mypy)
